// main64.cc is a part of the PYTHIA event generator.
// Copyright (C) 2023 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; LHE file

// This is a simple test program. It shows how PYTHIA 8 can write
// a Les Houches Event File v. 3.0 based on its process-level events.

#include "Pythia8/Pythia.h"
#include "Pythia8/LesHouches.h"
#include "Pythia8/HeavyIons.h"
#include "Pythia8Plugins/Pythia8Rivet.h"
#include "main93.h"
#include <chrono>
#ifdef PY8ROOT
#include "TTree.h"
#include "TFile.h"
#endif

using namespace Pythia8;

// Helper class to parse command line options.
class InputParser {

public:

  InputParser (int &argc, char **argv) {
    for (int i = 1; i < argc; ++i) arglist.push_back(string(argv[i]));
  }

  const string& getOption(const string &opt) const {
    vector<string>::const_iterator itr = find(arglist.begin(),
      arglist.end(), opt);
    if (itr != arglist.end() && ++itr != arglist.end()) return *itr;
    return "";
  }

  bool hasOption(const string &opt) const {
    return find(arglist.begin(), arglist.end(), opt) != arglist.end();
  }

private:
  vector<string> arglist;
};

class LHEF3StopStart: public LHAup {

public:

  // Constructor.
  LHEF3StopStart(Event* eventPtrIn, const Info* infoPtrIn,
    int pDigitsIn = 15, bool writeToFileIn = true) :
    eventPtr(eventPtrIn),infoPtr(infoPtrIn),
    particleDataPtr(infoPtrIn->particleDataPtr),
    settingsPtr(infoPtrIn->settingsPtr), writer(osLHEF),
    pDigits(pDigitsIn), writeToFile(writeToFileIn) {}

  // Routine for reading, setting and printing the initialisation info.
  bool setInit();

  // Routine for reading, setting and printing the next event.
  void setEventPtr(Event* evPtr) { eventPtr = evPtr; }
  bool setEvent(int = 0);
  string getEventString() { return writer.getEventString(&hepeup); }

  // Function to open the output file.
  bool openLHEF(string fileNameIn);

  // Function to close (and possibly update) the output file.
  bool closeLHEF(bool updateInit = false);

  // Some init and event block objects for convenience.
  HEPRUP heprup;
  HEPEUP hepeup;

  // Pointer to event that should be printed.
  Event* eventPtr;

  // Constant info pointer, explicitly overwrites member from LHAup base class.
  const Info* infoPtr;

  ParticleData* particleDataPtr;

private:

  // Pointer to settings and info objects.
  Settings* settingsPtr;

  // LHEF3 writer
  Writer writer;

  // Number of digits to set width of double write out
  int  pDigits;
  bool writeToFile;

};

bool LHEF3StopStart::setEvent(int ) {

    Event event = *eventPtr;

    // Begin filling Les Houches blocks.
    hepeup.clear();
    hepeup.resize(0);

    // The number of particle entries in the current event.
    hepeup.NUP = 2;
    for ( int i = 0; i < int(event.size()); ++i) {
      //if ( event[i].status() == -22) ++hepeup.NUP;
      if ( event[i].isFinal()) ++hepeup.NUP;
    }

    // The subprocess code for this event (as given in LPRUP).
    hepeup.IDPRUP = 9999;

    // The weight for this event.
    hepeup.XWGTUP = infoPtr->weight();

    // The PDF weights for the two incoming partons. Note that this
    // variable is not present in the current LesHouches accord
    // (<A HREF="http://arxiv.org/abs/hep-ph/0109068">hep-ph/0109068</A>),
    // hopefully it will be present in a future accord.
    hepeup.XPDWUP = make_pair(0,0);

    // The scale in GeV used in the calculation of the PDF's in this
    // event.
    //hepeup.SCALUP = eventPtr->scale();
    hepeup.SCALUP = infoPtr->settingsPtr->parm("TimeShower:pTmin");

    // The value of the QED coupling used in this event.
    hepeup.AQEDUP = infoPtr->alphaEM();

    // The value of the QCD coupling used in this event.
    hepeup.AQCDUP = infoPtr->alphaS();

    // Find incoming particles.
    int in1, in2;
    in1 = in2 = 0;
    for ( int i = 0; i < int( event.size()); ++i) {
      if ( event[i].mother1() == 1 && in1 == 0) in1 = i;
      if ( event[i].mother1() == 2 && in2 == 0) in2 = i;
    }

    // Find resonances in hard process.
    vector<int> hardResonances;
    /*
    for ( int i = 0; i < int(event.size()); ++i) {
      if ( event[i].status() != -22) continue;
      if ( event[i].mother1() != 3) continue;
      if ( event[i].mother2() != 4) continue;
      hardResonances.push_back(i);
    }
    */

    // Find resonances and decay products after showering.
    vector<int> evolvedResonances;
    vector<pair<int,int> > evolvedDecayProducts;
    /*
      for ( int j = 0; j < int(hardResonances.size()); ++j) {
      for ( int i = int(event.size())-1; i > 0; --i) {
        if ( i == hardResonances[j]
             || (event[i].mother1() == event[i].mother2()
                 && event[i].isAncestor(hardResonances[j])) ) {
          evolvedResonances.push_back(i);
          evolvedDecayProducts.push_back(
                                         make_pair(event[i].daughter1(), event[i].daughter2()) );
          break;
        }
      }
      }
    */

    // Event for bookkeeping of resonances.
    Event now  = Event();
    now.init("(dummy event)", particleDataPtr);
    now.reset();

    // The PDG id's for the particle entries in this event.
    // The status codes for the particle entries in this event.
    // Indices for the first and last mother for the particle entries in
    // this event.
    // The colour-line indices (first(second) is (anti)colour) for the
    // particle entries in this event.
    // Lab frame momentum (Px, Py, Pz, E and M in GeV) for the particle
    // entries in this event.
    // Invariant lifetime (c*tau, distance from production to decay in
    // mm) for the particle entries in this event.
    // Spin info for the particle entries in this event given as the
    // cosine of the angle between the spin vector of a particle and the
    // 3-momentum of the decaying particle, specified in the lab frame.
    hepeup.IDUP.push_back(event[in1].id());
    hepeup.IDUP.push_back(event[in2].id());
    hepeup.ISTUP.push_back(-1);
    hepeup.ISTUP.push_back(-1);
    hepeup.MOTHUP.push_back(make_pair(0,0));
    hepeup.MOTHUP.push_back(make_pair(0,0));
    hepeup.ICOLUP.push_back(make_pair(event[in1].col(),event[in1].acol()));
    hepeup.ICOLUP.push_back(make_pair(event[in2].col(),event[in2].acol()));
    vector <double> p;
    p.push_back(0.0);
    p.push_back(0.0);
    p.push_back(event[in1].pz());
    p.push_back(event[in1].e());
    p.push_back(event[in1].m());
    hepeup.PUP.push_back(p);
    p.resize(0);
    p.push_back(0.0);
    p.push_back(0.0);
    p.push_back(event[in2].pz());
    p.push_back(event[in2].e());
    p.push_back(event[in2].m());
    hepeup.PUP.push_back(p);
    p.resize(0);
    hepeup.VTIMUP.push_back(event[in1].tau());
    hepeup.VTIMUP.push_back(event[in2].tau());
    hepeup.SPINUP.push_back(event[in1].pol());
    hepeup.SPINUP.push_back(event[in2].pol());

    now.append(event[in1]);
    now.append(event[in2]);

    // Attach resonances
    for ( int j = 0; j < int(evolvedResonances.size()); ++j) {
      int i = evolvedResonances[j];
      hepeup.IDUP.push_back(event[i].id());
      hepeup.ISTUP.push_back(2);
      hepeup.MOTHUP.push_back(make_pair(1,2));
      hepeup.ICOLUP.push_back(make_pair(event[i].col(),event[i].acol()));
      p.push_back(event[i].px());
      p.push_back(event[i].py());
      p.push_back(event[i].pz());
      p.push_back(event[i].e());
      p.push_back(event[i].m());
      hepeup.PUP.push_back(p);
      p.resize(0);
      hepeup.VTIMUP.push_back(event[i].tau());
      hepeup.SPINUP.push_back(event[i].pol());
      now.append(event[i]);
      now.back().statusPos();
    }

    // Loop through event and attach remaining decays
    vector<int> iSkip;
    int iDec = 0;
    do {

      if ( now[iDec].isFinal() && now[iDec].canDecay()
           && now[iDec].mayDecay() && now[iDec].isResonance() ) {

        int iD1 = now[iDec].daughter1();
        int iD2 = now[iDec].daughter2();

        // Done if no daughters exist.
        if ( iD1 == 0 || iD2 == 0 ) continue;

        // Attach daughters.
        for ( int k = iD1; k <= iD2; ++k ) {
          Particle partNow = event[k];
          hepeup.IDUP.push_back(partNow.id());
          hepeup.MOTHUP.push_back(make_pair(iDec,iDec));
          hepeup.ICOLUP.push_back(make_pair(partNow.col(),partNow.acol()));
          p.push_back(partNow.px());
          p.push_back(partNow.py());
          p.push_back(partNow.pz());
          p.push_back(partNow.e());
          p.push_back(partNow.m());
          hepeup.PUP.push_back(p);
          p.resize(0);
          hepeup.VTIMUP.push_back(partNow.tau());
          hepeup.SPINUP.push_back(partNow.pol());
          now.append(partNow);
          if ( partNow.canDecay() && partNow.mayDecay() && partNow.isResonance()){
            now.back().statusPos();
            hepeup.ISTUP.push_back(2);
          } else
            hepeup.ISTUP.push_back(1);

          iSkip.push_back(k);
        }

        // End of loop over all entries.
      }
    } while (++iDec < now.size());

    // Attach final state particles
    for ( int i = 0; i < int(event.size()); ++i) {
      if (!event[i].isFinal()) continue;
      // Skip resonance decay products.
      bool skip = false;
      for ( int j = 0; j < int(evolvedDecayProducts.size()); ++j) {
        skip = ( i >= evolvedDecayProducts[j].first
                 && i <= evolvedDecayProducts[j].second);
      }
      if (skip) continue;
      for ( int j = 0; j < int(iSkip.size()); ++j) {
        skip = ( i == iSkip[j] );
      }
      if (skip) continue;

      hepeup.IDUP.push_back(event[i].id());
      hepeup.ISTUP.push_back(1);
      hepeup.MOTHUP.push_back(make_pair(1,2));
      hepeup.ICOLUP.push_back(make_pair(event[i].col(),event[i].acol()));
      p.push_back(event[i].px());
      p.push_back(event[i].py());
      p.push_back(event[i].pz());
      p.push_back(event[i].e());
      p.push_back(event[i].m());
      hepeup.PUP.push_back(p);
      p.resize(0);
      hepeup.VTIMUP.push_back(event[i].tau());
      hepeup.SPINUP.push_back(event[i].pol());
      now.append(event[i]);
    }

    // A pointer to the current HEPRUP object.
    hepeup.heprup = &heprup;

    // The weights associated with this event, as given by the LHAwgt tags.
    if (infoPtr->weights_detailed)
      hepeup.weights_detailed               = *(infoPtr->weights_detailed);

    // The weights associated with this event, as given by the LHAweights tags.
    if (infoPtr->weights_compressed)
      hepeup.weights_compressed             = *(infoPtr->weights_compressed);

    // Contents of the LHAscales tag
    if (infoPtr->scales)
      hepeup.scalesSave                     = *(infoPtr->scales);

    // Contents of the LHAweights tag (compressed format)
    if (infoPtr->weights)
      hepeup.weightsSave                    = *(infoPtr->weights);

    // Contents of the LHArwgt tag (detailed format)
    if (infoPtr->rwgt)
      hepeup.rwgtSave                       = *(infoPtr->rwgt);

    // Any other attributes.
    if (infoPtr->eventAttributes)
      hepeup.attributes                     = *(infoPtr->eventAttributes);

    // Not implemented yet:
    // Write event comments of input LHEF.

    writer.hepeup = hepeup;
    if (writeToFile) writer.writeEvent(&hepeup,pDigits);

    return true;

  };


bool LHEF3StopStart::openLHEF(string fileNameIn) {

  // Open file for writing. Reset it to be empty.
  fileName = fileNameIn;
  const char* cstring = fileName.c_str();
  osLHEF.open(cstring, ios::out | ios::trunc);
  if (!osLHEF) {
    cout << "Error in LHAup::openLHEF: could not open file "
         <<  fileName << endl;
    return false;
  }

  // Done.
  return true;
};

//--------------------------------------------------------------------------

// Routine for reading, setting and printing the initialisation info.

bool LHEF3StopStart::setInit() {

  // Start with clean writer.
  writer.headerStream.str("");
  writer.initStream.str("");
  writer.headerStream.clear();
  writer.initStream.clear();

  // PDG id's of beam particles. (first/second is in +/-z direction).
  heprup.IDBMUP = make_pair(infoPtr->idA(), infoPtr->idB());

  // Energy of beam particles given in GeV.
  heprup.EBMUP = make_pair(infoPtr->eA(),infoPtr->eB());

  // The author group for the PDF used for the beams according to the
  // PDFLib specification.
  heprup.PDFGUP = make_pair(0,0);

  // The id number the PDF used for the beams according to the
  // PDFLib specification.
  heprup.PDFSUP = make_pair(0,0);

  // Master switch indicating how the ME generator envisages the
  // events weights should be interpreted according to the Les Houches
  // accord.
  heprup.IDWTUP = -4;

  // The number of different subprocesses in this file.
  heprup.NPRUP = 1;

  // The cross sections for the different subprocesses in pb.
  vector<double> XSECUP;
  for ( int i=0; i < heprup.NPRUP; ++i)
    XSECUP.push_back(CONVERTMB2PB * (infoPtr->sigmaGen()));
  heprup.XSECUP = XSECUP;

  // The statistical error in the cross sections for the different
  // subprocesses in pb.
  vector<double> XERRUP;
  for ( int i=0; i < heprup.NPRUP; ++i)
    XERRUP.push_back(CONVERTMB2PB * infoPtr->sigmaErr());
  heprup.XERRUP = XERRUP;

  // The maximum event weights (in HEPEUP::XWGTUP) for different
  vector<double> XMAXUP;
  for ( int i=0; i < heprup.NPRUP; ++i) XMAXUP.push_back(0.0);
  heprup.XMAXUP = XMAXUP;

  // The subprocess code for the different subprocesses.
  vector<int> LPRUP;
  for ( int i=0; i < heprup.NPRUP; ++i) LPRUP.push_back(9999+i);
  heprup.LPRUP = LPRUP;

  // Contents of the LHAinitrwgt tag
  if (infoPtr->initrwgt )
    heprup.initrwgt = *(infoPtr->initrwgt);

  // Contents of the LHAgenerator tags.
  if (infoPtr->generators)
    heprup.generators = *(infoPtr->generators);

  // A map of the LHAweightgroup tags, indexed by name.
  if (infoPtr->weightgroups)
    heprup.weightgroups = *(infoPtr->weightgroups);

  // A map of the LHAweight tags, indexed by name.
  if (infoPtr->init_weights)
    heprup.weights = *(infoPtr->init_weights);

  // Get init information.
  writer.version = 3;

  string line, tag;

  // Not implemented yet:
  // Write header block of input LHEF
  // Write header comments of input LHEF

  // Print Pythia settings
  stringstream setout;
  settingsPtr->writeFile(setout, false);
  while ( getline(setout,line) )
    writer.headerBlock() << line << "\n";

  // Not implemented yet:
  // Write init comments of input LHEF.

  writer.heprup = heprup;
  writer.init();

  // Done
  return true;
};

//--------------------------------------------------------------------------

// Routine for reading, setting and printing the next event.


bool LHEF3StopStart::closeLHEF(bool updateInit) {

  // Write an end to the file.
  osLHEF << "</LesHouchesEvents>" << endl;
  osLHEF.close();

  // Optionally update the cross section information.
  if (updateInit) {
    const char* cstring = fileName.c_str();
    osLHEF.open(cstring, ios::in | ios::out);

    setInit();
    osLHEF.close();
  }

  // Done.
  return true;

};

//==========================================================================

int main(int argc, char* argv[]) {

  // Parser object for command line input.
  InputParser ip(argc, argv);

  // Print help text and exit.
  if(ip.hasOption("-h") || ip.hasOption("--help")) {
    cout << "Usage: Run Pythia with cmnd file input, and get Rivet, HepMC or\n"
      "standard Pythia output.\n" << endl;
    cout << "Examples:\n\n\t ./main93 [options] \n\n\t or\n\n\t ./main93 -c "
      "main93.cmnd -n 1000 -o myoutput\n" << endl;
    cout << "Options:\n"
      "\t -h, --help\n\t\t Show this help message and exit.\n"
      "\t -c CMND-FILE\n\t\t Use this user-written command file.\n"
      "\t -c2 CMND-FILE2\n\t\t Use a second cmnd file, loaded after "
       "the first.\n"
      "\t \t Useful for eg. tuning studies.\n"
      "\t -s SEED \n\t\t Specify seed for the random number generator.\n"
      "\t -o OUT \n\t\t Specify output prefix. Rivet histograms becomes "
      "OUT.yoda.\n"
      "\t -n NEVENTS\n\t\t Number of events.\n"
      "\t -l \n\t\t Silence the splash screen.\n"
      "\t -t \n\t\t Time event generation.\n"
      "\t -v \n\t\t Print the Pythia version number and exit.\n"
        << endl;
     cout << "Additional options in cmnd file:\n"
       "A few extra commands can be added to the cmnd file, compared "
       "to normal.\n"
       "\t Main:runRivet = on \n\t\tRun Rivet analyses (requires a\n"
       "\t\tworking installation of Rivet, linked to main93).\n"
       "\t Main:analyses = ANALYSIS1,ANALYSIS2,...\n "
       "\t\tA comma separated list of desired Rivet analyses to be run.\n"
       "\t\tAnalyses can be post-fixed with Rivet analysis parameters:\n"
        "\t\tANALYSIS:parm->value:parm2->value2 etc.\n"
       "\t Main:rivetRunName = STRING \n\t\tAdd an optional run name to "
       "the Rivet analysis.\n"
       "\t Main:rivetIgnoreBeams = on\n\t\tIgnore beams in Rivet. \n"
       "\t Main:rivetDumpPeriod = NUMBER\n\t\tDump Rivet histograms "
       "to file evert NUMBER of events.\n"
       "\t Main:rivetDumpFile = STRING\n\t\t Specify alternative "
       "name for Rivet dump file. Default = OUT.\n"
       "\t Main:writeHepMC = on \n\t\tWrite HepMC output (requires "
       "a linked installation of HepMC).\n"
       "\t Main:writeRoot = on \n\t\tWrite a root tree defined in the "
       "main93.h header file.\n\t\tRequires a working installation of Root, "
       "linked to Pythia.\n"
       "\t Main:outputLog = on\n\t\tRedirect output to a logfile. Default is "
       "OUT prefix, ie. pythia.log.\n"
         << endl;
    return 0;
  }

  // Print version number and exit.
  if(ip.hasOption("-v") || ip.hasOption("--version")) {
    cout << "PYTHIA version: " << PYTHIA_VERSION << endl;
    return 0;
  }

  string cmndfile = "";
  // Input command file.
  if(ip.hasOption("-c")) {
    cmndfile = ip.getOption("-c");
    if(cmndfile.find(".cmnd") == string::npos &&
        cmndfile.find(".dat") == string::npos) {
      cout << "Please provide a valid .cmnd file as "
      "argument to the -c option." << endl;
      return 1;
    }
  }
  else {
    cout << "You must provide a command file to produce output.\n"
            "Use option -h to show all command line options." << endl;
    return 1;
  }

  string out = "";
  // Set individual output prefix.
  if(ip.hasOption("-o")) out = ip.getOption("-o");

  // Set up for external input of LHEF 3.0 events.
  Pythia pythia;
  // pythia.readFile("main65.cmnd");
  pythia.readFile(cmndfile);
  pythia.init();

  // Create and open file for LHEF 3.0 output.
  LHEF3StopStart myLHEF3(&pythia.event, &pythia.info);
  myLHEF3.openLHEF((out == "" ? "stop_start.lhe" : out + ".lhe"));

  // Write out initialization info on the file.
  myLHEF3.setInit();

  // Set up hepmc file:
  Pythia8ToHepMC ToHepMC;
  ToHepMC.setNewFile((out == "" ? "stop_start.hepmc" : out + ".hepmc"));

  int nEvent = pythia.settings.mode("Main:numberOfEvents");

  // Event generation loop.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    // Generate next event.
    if (!pythia.next()) {
      if( pythia.info.atEndOfFile() ) break;
      else continue;
    }

    // Store and write event info.
    myLHEF3.setEvent();
    ToHepMC.writeNextEvent(pythia);

  } // End loop over events to generate.

  // Statistics: full printout.
  pythia.stat();

  // Write endtag. Overwrite initialization info with new cross sections.
  myLHEF3.closeLHEF(true);

  // Done.
  return 0;
}
