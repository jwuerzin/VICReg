#!/bin/bash

# Get name of evgendir and move there:
EVGENDIR=$(dirname "$(readlink -f "$0")"); cd $EVGENDIR

echo "Testing if conda is setup correctly..."
conda info | grep "vicreg" > /dev/null 2>&1
if ! [ $? -eq 0 ] ; then
    echo "Currently NOT in correct conda environment. Exiting."
    exit 1
else
    echo "conda setup correctly. Proceeding."
fi

echo "Building RIVET..."
mkdir -p rivet && cd rivet &&
wget -O rivet-bootstrap https://gitlab.com/hepcedar/rivetbootstrap/raw/3.1.8/rivet-bootstrap && chmod +x rivet-bootstrap &&
YODA_CONFFLAGS="--with-zlib=/opt/anaconda3" RIVET_CONFFLAGS="--with-zlib=/opt/anaconda3" MAKE="make -j8" ./rivet-bootstrap &&
source local/rivetenv.sh &&
rivet --list-analyses

if ! [ $? -eq 0 ] ; then
    echo "Problem with rivet build. Exiting."
    exit 1
else
    echo "rivet built successfully. Proceeding."
fi

echo "Building pythia..."
cd $EVGENDIR &&
tar xfz pythia8307.tgz; cd pythia8307 &&
./configure --with-rivet &&
make -j8 &&
cd examples && cp ../../Inputs_Steve/* . &&
make main1021 && make main93 && make main65 &&
./main1021

if ! [ $? -eq 0 ] ; then
    echo "Problem with pythia build. Exiting."
    exit 1
else
    echo "pythia built successfully. Proceeding."
fi