import os
import awkward as ak
import vector
vector.register_awkward()
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np

from optparse import OptionParser
myInputParser = OptionParser()
myInputParser.add_option("--ptcut", dest='ptcut', default="5.0", type=str, help="ptcut to run over.")
(options, args) = myInputParser.parse_args()  

# Sort input directories:
ptlist = os.listdir("/remote/ceph/user/w/wuerzing//VICReg/output/")

def get_number_and_subnum(filename):
    parts = filename.split('_')
    if len(parts) == 2:
        return float(parts[0]), int(parts[1])
    else:
        return 0, 0

ptlist = sorted(ptlist, key=get_number_and_subnum)
ptlist = [pt for pt in ptlist if options.ptcut in pt]

aklist = []
aklist_partial = []

def TwoDhist(x, y, weights, filen, xlab, ylab, title, binsetting=80, density=True):
    # Create a histogram
    sns.set(style="whitegrid")  # Set the style of the plot
    plt.figure(figsize=(10, 8))  # Set the figure size
    
    if density:
            hist = sns.histplot(x=x, y=y, weights=weights, bins=binsetting, cmap='viridis', cbar=True, stat='density', norm=LogNorm(), vmin=None, vmax=None, binrange=[[-5,5],[-np.pi,np.pi]])
    else:
            hist = sns.histplot(x=x, y=y, weights=weights, bins=binsetting, cmap='viridis', cbar=True, vmin=None, vmax=None, binrange=[[-5,5],[-np.pi,np.pi]])
        

    # Customize the plot
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel(ylab)

    cbar = hist.collections[0].colorbar
    cbar.set_label("pT Density" if density else "pT", rotation=90, labelpad=15)

    # Show the plot
    print(f"Saving plot: {filen}")
    plt.savefig(filen)
    plt.close()

i = 0
for pt in ptlist:
    datadir = f"{os.environ['DATA']}VICReg/output/{pt}"

    akfilen_reco = f"{datadir}/akarr_reco.parquet"
    # akfilen_partial_reco = f"{datadir}/akarr_partial_reco.parquet"

    print(f"Reading jets from: {akfilen_reco}")
    # print(f"Reading jets partial from: {akfilen_partial_reco}")
    if i == 0:
        jets = ak.from_parquet(akfilen_reco)
        # jets_partial = ak.from_parquet(akfilen_partial_reco)
    else:
        jets = ak.concatenate( (jets, ak.from_parquet(akfilen_reco)), axis=-1)
        # jets_partial = ak.concatenate( (jets_partial, ak.from_parquet(akfilen_partial_reco)), axis=-1)
    
    i += 1

# Opening message:
print(f"Running with options: {options}")

print(f"Converting jets in partial event to eta-phi coordinates:")
jets.behavior = vector.backends.awkward.behavior
convjets = jets.to_rhophietatau()

# jets_partial.behavior = vector.backends.awkward.behavior
# convjets_partial = jets_partial.to_rhophietatau()

# print(ak.flatten(convjets['eta']), ak.flatten(convjets['phi']))
# print(convjets_partial)

# Plot pT density for all jets:
TwoDhist(ak.flatten(convjets['eta']), ak.flatten(convjets['phi']), ak.flatten(convjets['rho']), f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_alljets_eta_phi_{options.ptcut}.png', 'eta', 'phi', f'alljet pT density over all events for ptcut: {options.ptcut}')
# Make event displays for single completions:
TwoDhist(convjets['eta'][0], convjets['phi'][0], convjets['rho'][0], f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_alljets_eta_phi_{options.ptcut}_ev0.png', 'eta', 'phi', f'alljet pT for ev0, ptcut: {options.ptcut}', density=False)
TwoDhist(convjets['eta'][1], convjets['phi'][1], convjets['rho'][1], f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_alljets_eta_phi_{options.ptcut}_ev1.png', 'eta', 'phi', f'alljet pT for ev1, ptcut: {options.ptcut}', density=False)

# Plot pT density for selected jets:
seljets = convjets[convjets['rho'] > 5.0]
TwoDhist(ak.flatten(seljets['eta']), ak.flatten(seljets['phi']), ak.flatten(seljets['rho']), f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_jets_eta_phi_{options.ptcut}.png', 'eta', 'phi', f'jet pT density over all events for ptcut: {options.ptcut}')
# Make event displays for single completions, selected jets:
TwoDhist(seljets['eta'][0], seljets['phi'][0], seljets['rho'][0], f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_jets_eta_phi_{options.ptcut}_ev0.png', 'eta', 'phi', f'jet pT for ev0, ptcut: {options.ptcut}', density=False)
TwoDhist(seljets['eta'][1], seljets['phi'][1], seljets['rho'][1], f'/home/iwsatlas1/wuerzing/VICReg/run/output/2dhists/2dhist_jets_eta_phi_{options.ptcut}_ev1.png', 'eta', 'phi', f'jet pT for ev1, ptcut: {options.ptcut}', density=False)