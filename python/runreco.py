import numpy as np
import pandas as pd
import os
from optparse import OptionParser
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import time

import pyhepmc
import seaborn as sns
import matplotlib.pyplot as plt
from particle import Particle

import awkward as ak
import fastjet
jetdef = fastjet.JetDefinition(fastjet.antikt_algorithm, 0.6)

start_time = time.time()

myInputParser = OptionParser()
myInputParser.add_option("--ptcut", dest='ptcut', default="5.0", type=str, help="ptcut to run over.")
myInputParser.add_option("--count", dest='count', default="1", type=str, help="count (run) to run over.")
myInputParser.add_option("--makeakarr", action="store_true", dest='makeakarr', default=False, help="Re-build the akarr from .hepmc files.")
myInputParser.add_option("--nruns", dest='nruns', default=None, type=int, help="Number of pythia runs to run over.")
(options, args) = myInputParser.parse_args()  

datadir = f"{os.environ['DATA']}VICReg/output/forML/{options.ptcut}/{options.count}"
akfilen_truth_1 = f"{datadir}/akarr_truth_1.parquet"
akfilen_truth_2 = f"{datadir}/akarr_truth_2.parquet"
akfilen_partial_truth = f"{datadir}/akarr_partial_truth.parquet"
akfilen_reco_1 = f"{datadir}/akarr_reco_1.parquet"
akfilen_reco_2 = f"{datadir}/akarr_reco_2.parquet"
akfilen_partial_reco = f"{datadir}/akarr_partial_reco.parquet"

aklist_1 = []
aklist_2 = []
aklist_partial = []

def printrun(string):
    print(f"pTcut: {options.ptcut}, count: {options.count} | {string}")
    return None
    # Create a histogram
    sns.set(style="whitegrid")  # Set the style of the plot
    plt.figure(figsize=(8, 6))  # Set the figure size
    
    if binsetting == 'int':
        sns.histplot(x, bins=np.linspace(np.min(x)-0.5, np.max(x)+0.5, int(np.max(x) - np.min(x) + 2 )), color='skyblue')  # Create the histogram
    else:
        sns.histplot(x, bins=binsetting, color='skyblue')

    # Customize the plot
    plt.title(title)
    plt.xlabel(xlab)
    plt.ylabel("Frequency")

    # Show the plot
    # printrun(f"Saving plot: {filen}")
    plt.savefig(filen)
    plt.close()

def saveakarr(akarr, filen):
    printrun(f"Saving akarray to: {filen}")
    ak.to_parquet(akarr, filen)

def readakarr(filen):
    printrun(f"Reading akarray from: {filen}")
    return ak.from_parquet(filen)

def makeakarr(filepath):
    with pyhepmc.open(filepath) as reader:
        event = reader.read()
        parts = event.numpy.particles
        
        akarr = ak.Array({
            'pid': parts.pid,
            'generated_mass': parts.generated_mass,
            'pid': parts.pid,
            'px': parts.px,
            'py': parts.py,
            'pz': parts.pz,
            'E': parts.e,
            'status': parts.status,
        })
        
        reader.close()
        # Only save final state particles:
    return akarr[akarr['status'] == 1]

if options.makeakarr:
    printrun("Making truth akarr!")

    # Defining number of files to run over:
    numfiles = len(os.listdir(f"{datadir}/partialruns_hepmc"))
    nruns = min(numfiles, options.nruns) if options.nruns else numfiles
    
    for run in range(1, nruns + 1):
        if run%100==0:
            printrun(f"Filling for run: {options.ptcut}, {run}")
        
        # Making akarrs for partial events:
        filen_partial = f'stop_start_{options.ptcut}_{options.count}_{run}.hepmc'
        aklist_partial += [ makeakarr(f"{datadir}/partialruns_hepmc/{filen_partial}") ] 

        # Making akarrs for finished events:
        filen_1 = f'finishedruns_{run}.hepmc'
        aklist_1 += [makeakarr(f"{datadir}/finishedruns_1/{filen_1}")]
        filen_2 = f'finishedruns_{run}.hepmc'
        aklist_2 += [makeakarr(f"{datadir}/finishedruns_2/{filen_2}")]

    # Convert lists to akarrays:
    aktruth_partial = ak.Array(aklist_partial)
    aktruth_1 = ak.Array(aklist_1)
    aktruth_2 = ak.Array(aklist_2)
    
    end_time = time.time()
    elapsed_time = end_time - start_time
    printrun(f"Making akarr took: {elapsed_time} seconds")

    # Save akarrays:
    saveakarr(aktruth_partial, akfilen_partial_truth)
    saveakarr(aktruth_1, akfilen_truth_1)
    saveakarr(aktruth_2, akfilen_truth_2)
else:
    aktruth_partial = readakarr(akfilen_partial_truth)
    aktruth_1 = readakarr(akfilen_truth_1)
    aktruth_2 = readakarr(akfilen_truth_2)

# Opening message:
printrun(f"Running with options: {options}")

# Reconstruct jets:
printrun("Reconstructing jets for partial and completed events:")
jets_partial = fastjet.ClusterSequence(aktruth_partial, jetdef).inclusive_jets()
saveakarr(jets_partial, akfilen_partial_reco)

jets_1 = fastjet.ClusterSequence(aktruth_1, jetdef).inclusive_jets()
jets_2 = fastjet.ClusterSequence(aktruth_2, jetdef).inclusive_jets()
saveakarr(jets_1, akfilen_reco_1)
saveakarr(jets_2, akfilen_reco_2)