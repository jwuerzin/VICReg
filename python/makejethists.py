import awkward as ak
import os
from optparse import OptionParser
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

myInputParser = OptionParser()
myInputParser.add_option("--subset", dest='subset', default='', type=str, help="Subset of pT directories to run over")
(options, args) = myInputParser.parse_args()  

# Sort input directories:
ptlist = os.listdir("/remote/ceph/user/w/wuerzing//VICReg/output/")

def get_number_and_subnum(filename):
    parts = filename.split('_')
    if len(parts) == 2:
        return float(parts[0]), int(parts[1])
    else:
        return 0, 0

ptlist = sorted(ptlist, key=get_number_and_subnum)

suffix = ''
if options.subset != '':
    ptlist = [pt for pt in ptlist if options.subset in pt]
    suffix += f'_{options.subset}'

aklist = []
for i in range(len(ptlist)):
# for i in range(5):
    jetsfile = f"/remote/ceph/user/w/wuerzing//VICReg/output/{ptlist[i]}/akarr_reco.parquet"
    print(f"Reading jets from: {jetsfile}")
    aklist += [ak.from_parquet(jetsfile)]
    # Adding pT for akarr:
    aklist[i]['pt'] = np.sqrt(aklist[i]['px']**2+aklist[i]['py']**2)
    
print("Making list for ptsel:")
aklist_ptsel = [akarr[akarr['pt'] > 5.0] for akarr in aklist]

def makehists(aklist, x, filen, xlab, title, binsetting='auto'):
    
    # Make flat list of items for plotting:
    if xlab == 'multiplicity':
        plotlist = [ ak.num(akarr, axis=-1) for akarr in aklist ]
        binsetting = np.linspace(ak.min(plotlist), ak.max(plotlist), int(ak.max(plotlist) - ak.min(plotlist) + 2 ))
    else:
        plotlist = [ ak.flatten(akarr[x]) for akarr in aklist ]
    
    # Create pandas df for easier plotting:
    plotdf = pd.DataFrame(plotlist, index=[f"ptcut: {pt}" for pt in ptlist[:len(aklist)]]).T
    
    # Create a histogram
    sns.set(style="whitegrid")  # Set the style of the plot
    plt.figure(figsize=(8, 6))  # Set the figure size
    
    sns.histplot(
        plotdf,
        bins=binsetting,
        alpha=0.2,
        element='step'
        )

    # Customize the plot
    plt.title(title)
    plt.xlabel(xlab)
    plt.yscale('log')
    plt.ylabel("Frequency")

    # Show the plot
    print(f"Saving plot: {filen}")
    plt.savefig(filen)
    plt.close()

# Plot for all jets:
datadir = "/home/iwsatlas1/wuerzing/VICReg/run/output/alljetplots"
os.makedirs(f'{datadir}', exist_ok=True)
# plot hists for all fields in akarray:
for field in ak.fields(aklist[0]):
    makehists(aklist, field, f'{datadir}/hist_jets_{field}{suffix}.png', field, f"Histogram for jets", binsetting=50)

# plot jet multiplicity:
makehists(aklist, '', f'{datadir}/hist_jets_multiplicity{suffix}.png', "multiplicity", f"Histogram for jets")

# Plot for ptsel jets:
datadir = "/home/iwsatlas1/wuerzing/VICReg/run/output/jetplots"
os.makedirs(f'{datadir}', exist_ok=True)
# plot hists for all fields in akarray:
for field in ak.fields(aklist_ptsel[0]):
    makehists(aklist_ptsel, field, f'{datadir}/hist_jets_{field}{suffix}.png', field, f"Histogram for jets", binsetting=50)

# plot jet multiplicity:
makehists(aklist_ptsel, '', f'{datadir}/hist_jets_multiplicity{suffix}.png', "multiplicity", f"Histogram for jets")