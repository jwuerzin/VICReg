# VICReg

## Getting started

### Installation

Set up evgen by calling
```bash
cd evgen
./setup_evgen.sh
```

### On every login:

Setup by calling
```bash
source ~/.bashrc
conda activate vicreg
source rivet/local/rivetenv.sh
```

### Running:
Can run full data genneration by simply doing: 
```bash
./run/gendata.sh
```
Optionally, change ```NCORES```, ```PTCUTLIST```, ```COUNTLIST``` or ```NRUNS``` from their defaul values with:
```bash
NCORES={NCORES} PTCUTLIST={PTCUTLIST} COUNTLIST={COUNTLIST} NRUNS={NRUNS} ./gendata.sh
```

## Custom examples

The folloring examples have been provided by the Pythia authors. All are designed to be run inside ```examples``` directory and collected in ```Inputs_Steve```.

- ``` main1021.cc```: "It runs a shower down to a cutoff of 10 GeV, prints the event, then runs the shower
down to the normal cutoff, prints the event, then prints the whole hadronized event (but only
for the first event)."
- "The program ```main65.cc``` runs the shower to a pTmin set in ```main65.cmnd``` and outputs the events in 
the LHE format. The output LHE file can then be used with another main program (such as ```main93```) using
```testStopStart.cmnd```."

Random seeds can be enabled by adding the following options to the corresponding ```.cmnd``` file.
```bash
Random:setSeed = on
Random:seed = 0
```
**Careful!** The ```Random:seed = 0``` is based on time and may mess things up if pythia finishes too quickly...

## Small notes

Parallel running
``` bash
parallel -j3 "./main93 -c testStopStart.cmnd -n 1 -o finishedrun_{} && mv finishedrun_{}.hepmc ../../run/output/" ::: {1..10000}
```

HepMC particle status (according to https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookGenParticleCandidate):
| Code | Description |
| --- | --- |
| 0 | Null entry |
| 1 | Particle not decayed or fragmented, represents the final state as given by the generator |
| 2 | Decayed or fragmented entry (i.e. decayed particle or parton produced in shower.) |
| 3 | Identifies the "hard part" of the interaction, i.e. the partons used in the matrix element calculation, including immediate decays of resonances. (Documentation entry, defined separately from the event history. "This includes the two incoming colliding particles and partons produced in hard interaction." [ * ]) |
| 4-10 | Undefined, reserved for future standards |
| 11-200 | At the disposal of each model builder equivalent to a null line. Here: an intermediate (decayed/branched/...) particle that does not fulfill the criteria of status code 2, with a generator-dependent classification of its nature (see: http://hepmc.web.cern.ch/hepmc/releases/HepMC2_user_manual.pdf). |
| 201-... | At the disposal of the user, in particular for event tracking in the detector |

## ToDo:

- make plot like Fig12 in https://arxiv.org/pdf/1010.3698.pdf
- can we control hadronisation? i.e. shower until cutoff at 0.357 GeV and complete with fixed/random seed