#!/bin/bash
set -e 

VICREGDIR=$(dirname $(dirname "$(readlink -f "$0")"))

echo "Finishing Pythia run for: PTCUT: $PTCUT, COUNT: $COUNT, RUN: $RUN, FINISH: $FINISH"

# Make dir for output
mkdir -p $DATA/VICReg/output/forML/$PTCUT/$COUNT/finishedruns_$FINISH
mkdir -p $DATA/VICReg/output/forML/$PTCUT/$COUNT/finishedcommands_$FINISH

# Setup .cmnd file and execute:
cd $VICREGDIR/evgen/pythia8307/examples &&
cp testStopStart.cmnd testStopStart_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
sed -i "s/Random:seed = 0/Random:seed = $((100000*COUNT*FINISH+RUN))/" testStopStart_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
sed -i "s/Beams:LHEF = stop_start.lhe/Beams:LHEF = stop_start_"$PTCUT"_"$COUNT"_"$RUN".lhe/" testStopStart_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
./main93 -c testStopStart_"$PTCUT"_"$COUNT"_"$RUN".cmnd -n 1 -o $DATA/VICReg/output/forML/$PTCUT/$COUNT/finishedruns_$FINISH/finishedruns_$RUN &&
mv testStopStart_"$PTCUT"_"$COUNT"_"$RUN".cmnd $DATA/VICReg/output/forML/$PTCUT/$COUNT/finishedcommands_$FINISH