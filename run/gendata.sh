#!/bin/bash
set -e 

: ${NCORES="6"}
: ${NCOUNTS="100"}
: ${PTCUTLIST="5.0"}
# : ${PTCUTLIST="0.001 0.1 0.2 0.3 0.4 0.5 1.0 2.0 4.0 5.0 10.0 20.0 30.0"}
VICREGDIR=$(dirname $(dirname "$(readlink -f "$0")"))

commands=$(cat <<EOF
PTCUT={1} COUNT={2} $VICREGDIR/run/runpythia.sh
python $VICREGDIR/python/runreco.py --ptcut={1} --count={2} --makeakarr
EOF
)

echo $NRUNS
parallel -u -v -j$NCORES "$commands" ::: $PTCUTLIST ::: $(seq $NCOUNTS)