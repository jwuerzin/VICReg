#!/bin/bash
set -e 

: ${PTCUT:="5.0"}
: ${NCORES:="1"}
: ${NRUNS:="10000"}
: ${COUNT:="1"}
VICREGDIR=$(dirname $(dirname "$(readlink -f "$0")"))

# Setup rivet:
source $VICREGDIR/evgen/rivet/local/rivetenv.sh 

runncomplete=$(cat <<EOF
echo "RUNNING FOR $PTCUT"
# First partial run NRUN times:
RUN={} $VICREGDIR/run/startpythiarun.sh
# Finish partial run 2 times per run:
RUN={} FINISH=1 $VICREGDIR/run/finishpythiarun.sh
RUN={} FINISH=2 $VICREGDIR/run/finishpythiarun.sh
# Remove leftovers from startpythiarun.sh
rm $VICREGDIR/evgen/pythia8307/examples/stop_start_"$PTCUT"_"$COUNT"_"{}".*
EOF
)

parallel -u -v -j$NCORES "$runncomplete" ::: $(seq "$NRUNS")