#!/bin/bash
set -e 

VICREGDIR=$(dirname $(dirname "$(readlink -f "$0")"))

echo "Starting Pythia run for: PTCUT: $PTCUT, COUNT: $COUNT, RUN: $RUN"

# Make dir for output:
mkdir -p $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialruns_lhe
mkdir -p $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialruns_hepmc
mkdir -p $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialcommands

# Generate stop_start.lhe and .hepmc files to run over in the next step
cd $VICREGDIR/evgen/pythia8307/examples &&
cp main65.cmnd main65_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
sed -i "s/TimeShower:pTmin force 5.0/TimeShower:pTmin force $PTCUT/" main65_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
sed -i "s/Random:seed = 0/Random:seed = $((100000*COUNT+RUN))/" main65_"$PTCUT"_"$COUNT"_"$RUN".cmnd &&
./main65 -c main65_"$PTCUT"_"$COUNT"_"$RUN".cmnd -o stop_start_"$PTCUT"_"$COUNT"_"$RUN" &&
cp stop_start_"$PTCUT"_"$COUNT"_"$RUN".lhe $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialruns_lhe &&
mv stop_start_"$PTCUT"_"$COUNT"_"$RUN".hepmc $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialruns_hepmc &&
mv main65_"$PTCUT"_"$COUNT"_"$RUN".cmnd $DATA/VICReg/output/forML/$PTCUT/$COUNT/partialcommands